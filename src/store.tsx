import { createStore } from 'redux'
import { Player } from './globalStructures';
import PlayersReducer from './reducer';

export interface PlayersState {
    allPlayers: Player[],
    favouritePlayers: Player[]
}

export const playersState: PlayersState = {
    allPlayers: [],
    favouritePlayers: []
}

const store = createStore(PlayersReducer);

export default store;