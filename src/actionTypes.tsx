export const ADD_TO_FAVOURITE = "addToFavourite";
export const REMOVE_FROM_FAVOURITE = "removeFromFavourite";
export const ADD_INITIAL_JSON_DATA = "addInitialJsonData";