import { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import AddCircleOutlinedIcon from '@material-ui/icons/AddCircleOutlined';
import DeleteIcon from '@material-ui/icons/Delete';
import store from './store';
import * as actions from './actionTypes';
import { Player } from './globalStructures';
import AllPlayersList from './components/AllPlayersList';
import FavouritePlayersList from './components/FavouritePlayersList';
import Loader from 'react-loader-spinner';
import Swal from 'sweetalert2'
import SearchBar from "material-ui-search-bar";

interface PlayerResponse {
  id: number,
  first_name: string,
  last_name: string,
  team: {
    full_name: string
  }
}

function App() {
  const [isFetching, setIsFetching] = useState(true);
  const [playerName, setPlayerName] = useState("");
  let totalPages: number;

  async function loadPlayersFromPage(page: number) {
    const resPlayers = await fetch(`https://www.balldontlie.io/api/v1/players?page=${page}&per_page=100`);
    return resPlayers.json();
  }

  const savePlayersInStore = (playersAsJson: any) => {
    const players = playersAsJson.data.map((player: PlayerResponse) => {
      return new Player(player.id, player.first_name + " " + player.last_name, player.team.full_name)
    });
    store.dispatch({type: actions.ADD_INITIAL_JSON_DATA, payload: {players: players}});
  }

  /*
  The function loads players from first page + meta data about number of pages
  */
  async function firstLoadPlayers() {
    try {
      const playersAsJson = await loadPlayersFromPage(1);
      totalPages = playersAsJson.meta.total_pages;
      savePlayersInStore(playersAsJson);
      setIsFetching(totalPages !== 1);
    }
    catch(e) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Could not fetch all data for some reason. Wait a bit and try refresh.',
      });
    }
  }
  
  /* 
  The function loads the rest of the players based on the meta data from the first load
  */
  async function loadPlayers() {
    await firstLoadPlayers();
    for (let page = 2; page <= totalPages; page++) {
      try {
        savePlayersInStore(await loadPlayersFromPage(page));
      }
      catch(e) {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Could not fetch all data for some reason. Wait a bit and try refresh.'
        });
        break;
      }
    }
    setIsFetching(false);
  }

  useEffect(() => {
    loadPlayers();
  }, []);
  
  if (isFetching) {
    return (
      <div id="loader-wrapper">
        <p id="loader-text">Loading players</p>
        <div id="loader">
          <Loader type="ThreeDots" color="black" height="30" width="30"/>
        </div>
      </div>
    )
  }
  else {
    return (
      <div>
        <div id="head">
          <h1 className="text">Welcome to NBA Favourite</h1>
          <h2 className="text">Here you can pick nba players and put then in your list of favourites</h2>
        </div>
        <div id="search-bar">
          <SearchBar
              value={playerName}
              onChange={(newPlayerName: string) => {setPlayerName(newPlayerName)}}
              onCancelSearch={() => {setPlayerName("")}}
          />
        </div>
        <div id="body">
          <Grid container spacing={2}>
          <AllPlayersList 
          name="All" 
          actionIcon={<AddCircleOutlinedIcon />} 
          numberPerPage={100} 
          playerNameToSearch={playerName}
          />
          <FavouritePlayersList 
          name="Favourite" 
          actionIcon={<DeleteIcon />} 
          numberPerPage={100} />
          </Grid>
        </div>
      </div>
    )
  }
}

export default App;
