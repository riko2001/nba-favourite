import { ListItem, ListItemAvatar, ListItemText, ListItemSecondaryAction, IconButton } from "@material-ui/core";
import { Player } from "../globalStructures";
import SportsBasketballTwoToneIcon from '@material-ui/icons/SportsBasketballTwoTone';

interface PlayerListItemProps {
    player: Player,
    actionIcon: JSX.Element,
    onPlayerClick: any
}

export default function PlayerListItem(props: PlayerListItemProps) {
    return (
        <ListItem>
            <ListItemAvatar>
                <SportsBasketballTwoToneIcon />
            </ListItemAvatar>
            <ListItemText
              primary={props.player.name}
              secondary={props.player.team}
            />
            <ListItemSecondaryAction>
              <IconButton edge="end" aria-label="add" onClick={() => {props.onPlayerClick(props.player.id)}}>
                {props.actionIcon}
              </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    )
} 