import List from '@material-ui/core/List';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import store from "../store";
import PlayerListItem from './PlayerListItem';
import { PlayersListProps } from '../globalStructures';
import Paginator from './Paginator';
import { useState } from 'react';
import { ADD_TO_FAVOURITE } from '../actionTypes';
import { useSelector } from 'react-redux';
import { PlayersState } from '../store';
import { useEffect } from 'react';

interface AllPlayersListProps extends PlayersListProps {
  playerNameToSearch: string
}

export default function AllPlayersList(props: AllPlayersListProps) {
  const allPlayers = useSelector<PlayersState, PlayersState["allPlayers"]>(
    (state) => state.allPlayers
  );

  const [filteredPlayers, setFilteredPlayers] = useState(allPlayers);

  const [pagination, setPagination] = useState({
    offset: 0,
    pageCount: filteredPlayers.length / props.numberPerPage
  });

  const handlePageClick = (event: any) => {
    const selected = event.selected;
    const offset = selected * props.numberPerPage;
    setPagination({ ...pagination, offset});
  };

  const addToFavourite = (id: number) => {
    store.dispatch({type: ADD_TO_FAVOURITE, payload: {playerID: id}});
  };

  /*
  Every time playerNameToSearch is changed,
  filter the players based on this value
  and set the pagination accordingly
  */
  useEffect(() => {
    const playerNameLowerCase = props.playerNameToSearch.toLowerCase();
    const filtered = allPlayers.filter((player) => player.name.toLowerCase().includes(playerNameLowerCase));
    setFilteredPlayers(filtered);
    setPagination({ ...pagination, offset: 0, pageCount: filtered.length / props.numberPerPage});
  }, [props.playerNameToSearch]);

  return (
    <Grid item xs={12} sm={6}>
        <Typography variant="h4">
          {props.name}
        </Typography>
        <div className="players-list-wrapper">
          <List className="players-list">
          {filteredPlayers.slice(pagination.offset, pagination.offset + props.numberPerPage).map((player) => {
            return <PlayerListItem 
                    key={player.id} 
                    player={player} 
                    actionIcon={props.actionIcon} 
                    onPlayerClick={addToFavourite}></PlayerListItem>
          })}
          </List>
          <Paginator 
          numberPerPage={props.numberPerPage}
          pageCount={pagination.pageCount}
          onPageClick={handlePageClick} />
        </div>
    </Grid>
  )
}