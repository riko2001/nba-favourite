import List from '@material-ui/core/List';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import store from "../store";
import PlayerListItem from './PlayerListItem';
import { PlayersListProps } from '../globalStructures';
import Paginator from './Paginator';
import { useState } from 'react';
import { REMOVE_FROM_FAVOURITE } from '../actionTypes';
import { PlayersState } from '../store';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';

export default function FavouritePlayersList(props: PlayersListProps) {
  const favouritePlayers = useSelector<PlayersState, PlayersState["favouritePlayers"]>(
    (state) => state.favouritePlayers
  );

  const [pagination, setPagination] = useState({
    offset: 0,
    pageCount: 1
  });

  const [backgroundColor, setBackgroundColor] = useState("white");

  /*
  Every time favouritePlayers is changed,
  the pagination need to be updated accrodingly
  */
  useEffect(() => {
    const pageCount = Math.max(Math.ceil(favouritePlayers.length / props.numberPerPage), 1);
    setPagination({...pagination, pageCount});
  }, [favouritePlayers]);

  const handlePageClick = (event: any) => {
    const selected = event.selected;
    const offset = selected * props.numberPerPage;
    setPagination({...pagination, offset});
  }

  const removeFromFavourite = (id: number) => {
    store.dispatch({type: REMOVE_FROM_FAVOURITE, payload: {playerID: id}});
  };

  return (
    <Grid item xs={12} sm={6}>
        <Typography variant="h4">
          {props.name}
        </Typography>
        <div style={{backgroundColor: backgroundColor}}>
          <List className="players-list">
          <input 
          type="color" 
          id="colorPicker" 
          onChange={(event) => {setBackgroundColor(event.target.value)}} />
          {favouritePlayers.slice(pagination.offset, pagination.offset + props.numberPerPage).map((player) => {
            return <PlayerListItem 
                    key={player.id} 
                    player={player} 
                    actionIcon={props.actionIcon}
                    onPlayerClick={removeFromFavourite}></PlayerListItem>
          })}
          </List>
          <Paginator 
            numberPerPage={props.numberPerPage}
            pageCount={pagination.pageCount}
            onPageClick={handlePageClick} />
        </div>
    </Grid>
  )
}
