import ReactPaginate from 'react-paginate';

interface PaginatorProps {
    numberPerPage: number,
    pageCount: number,
    onPageClick: any
}

export default function Paginator(props: PaginatorProps) {
    return (
        <ReactPaginate
        previousLabel={'previous'}
        nextLabel={'next'}
        breakLabel={'...'}
        pageCount={props.pageCount}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={(event) => {
            props.onPageClick(event)
        }}
        containerClassName={'pagination'}
        activeClassName={'active'}
        />
    )
}