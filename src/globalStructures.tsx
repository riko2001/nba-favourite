export class Player {
    constructor(public id: number, public name: string, public team: string) {}
}

export interface PlayersListProps {
    name: string,
    actionIcon: JSX.Element,
    numberPerPage: number
}