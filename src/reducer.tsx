import * as actions from "./actionTypes";
import { playersState } from "./store";
import Swal from 'sweetalert2';

export default function playersReducer(state = playersState, action: any) {
    switch (action.type) {
      case actions.ADD_TO_FAVOURITE:
        // check if player already exists in favourites
        if (state.favouritePlayers.filter(player => player.id === action.payload.playerID).length === 1) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Player is already in favourites',
          })
          return state;
        }
        return {
          ...state,
          favouritePlayers: state.favouritePlayers.concat(
            state.allPlayers.filter(player => player.id === action.payload.playerID)
          )
        };
      case actions.REMOVE_FROM_FAVOURITE:
        return {
          ...state,
          favouritePlayers: state.favouritePlayers.filter(player => player.id !== action.payload.playerID)
        };
      case actions.ADD_INITIAL_JSON_DATA:
        return {
          ...state,
          allPlayers: state.allPlayers.concat(action.payload.players)
        };
      default:
        return state;
    }
}