import * as actions from './actionTypes';
import { Player } from './globalStructures';

export const addToFavourite = (playerID: number) => ({
    type: actions.ADD_TO_FAVOURITE,
    payload: {
        playerID
    }
})

export const removeFromFavourite = (playerID: number) => ({
    type: actions.REMOVE_FROM_FAVOURITE,
    payload: {
        playerID
    }
})

export const addInitialJsonData = (players: Player[]) => ({
    type: actions.ADD_INITIAL_JSON_DATA,
    payload: {
        players
    }
})